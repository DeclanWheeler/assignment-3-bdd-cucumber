package com.ait.example;

public class Member {

    private int numberOfTicketsBoughtLastSeason;

    public boolean qualifiesFor(Show game) {
        return numberOfTicketsBoughtLastSeason >= game.getLevel().getRequiredNumberOfTicketsBoughtLastSeason();
    }

    public void setNumberOfTicketsBoughtLastSeason(int ticketsBoughtLastSeason) {
        this.numberOfTicketsBoughtLastSeason = ticketsBoughtLastSeason;
    }
    public int getNumberOfTicketsBoughtLastSeason() {
        return numberOfTicketsBoughtLastSeason;
    }
}
